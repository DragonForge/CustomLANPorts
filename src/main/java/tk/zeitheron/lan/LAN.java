package tk.zeitheron.lan;

import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.fml.loading.FMLPaths;
import tk.zeitheron.hammerlib.util.cfg.ConfigFile;
import tk.zeitheron.hammerlib.util.cfg.entries.ConfigEntryCategory;

import java.io.File;

import static tk.zeitheron.lan.BetterShareToLanScreen.MAX_PORT;

@Mod("lan")
public class LAN
{
	public LAN()
	{
		FMLJavaModLoadingContext.get().getModEventBus().register(this);
		MinecraftForge.EVENT_BUS.register(this);
	}

	public static ConfigFile configs;

	@SubscribeEvent
	public void setup(FMLCommonSetupEvent e)
	{
		File file = new File(FMLPaths.CONFIGDIR.get().toFile(), "lan.hcfg");
		configs = new ConfigFile(file);
		LANConfigs.reload();
	}

	public static class LANConfigs
	{
		public static int port, maxPlayers;
		public static String motd;

		public static void reload()
		{
			ConfigEntryCategory data = configs.getCategory("Data");

			port = data.getIntEntry("Port", 25565, 0, MAX_PORT).setDescription("LAN port on which the world would open.").getValue();
			maxPlayers = data.getIntEntry("MaxPlayers", 8, 0, Short.MAX_VALUE).setDescription("Maximal amount of players to let in.").getValue();
			motd = data.getStringEntry("MOTD", "").setDescription("Message of the Day (or second line in server list)").getValue();

			if(configs.hasChanged()) configs.save();
		}

		public static void save()
		{
			ConfigEntryCategory data = configs.getCategory("Data");

			data.getIntEntry("Port", 25565, 0, Short.MAX_VALUE).setDescription("LAN port on which the world would open.").setValue(port);
			data.getIntEntry("MaxPlayers", 8, 0, Short.MAX_VALUE).setDescription("Maximal amount of players to let in.").setValue(maxPlayers);
			data.getStringEntry("MOTD", "").setDescription("Message of the Day (or second line in server list)").setValue(motd);

			if(configs.hasChanged()) configs.save();
		}
	}
}